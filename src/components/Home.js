import React from 'react';
import SlideShow from './slideshow/SlideShow';
import SoundPlayer from './SoundPlayer';
import soundtrack from './TheEcstasyOfGold.mp3';

const Home = () => {
    return (
        <div>
            <div align="center" >
                <button className="controls" id="previous">Previous</button>
                <button className="controls" id="pause">Pause</button>
                <button className="controls" id="next">Next</button>
                <div>
                    <button className="controls" id="restart">Start at beginning</button>
                    <SoundPlayer src={soundtrack}></SoundPlayer>
                </div>
            </div >
            <div align="center">
                <SlideShow />
            </div>
        </div >
    );
};

export default Home;