
import React from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { resetSubscription, createSubscriber } from '../actions';


class SignUp extends React.Component {
    disableSubmit = () => {
        const signupSubmit = document.getElementById('register');
        signupSubmit.className = 'dimSignupSubmit';
    };

    componentDidMount = () => {
        this.disableSubmit();
    }

    //The problem:
    //  I need to be able to tell the user that their newsletter registration succeeded if they are a new subscriber, OR
    //  that they are already a subscriber (maybe they forgot), so createNewSubscription will be false.
    //  But, we will arrive here at componentDidUpdate for any state changes, not just the registration process.
    //  So that is the reason for the 'fetchSubscriptionAttempted' check.
    //  Once we know we are here as a result of a registration attempt, we can check to see if they got registered (if new)
    //  or did not get registered (because already are registered).

    //unexpected complication: async await network calls are not updating the state in time for me to know what happened in componentDidUpdate.
    //Originally I tried to make 'createSubscriber' action call from here, but I could not evaluate the bools here. So I moved 'createSubscriber' elsewhere.

    //We get here for all changes in state in the application. So we get here a ton. Thats why I
    //am checking for the specific state changes I care about.
    //We get here for various other reasons caused by redux-form, like when focus is lost on a field.
    //Also, if I try to perform a network request using async await, I get here multiple times without the
    //state changing until the last network call, which is a huge problem because these boolean conditions
    //(like fetchSubscriptionAttempted) are not being set correctly for async network calls.
    // Only non async await Network calls work fine here by changing the state consistently.
    componentDidUpdate = () => {
        if (this.props.subscriber.fetchSubscriptionAttempted === true) {
            this.subscription();
        }
    }

    subscription = () => {
        if (this.props.subscriber.createNewSubscription === true) {
            toast.success(`successful newsletter registration for: ${this.props.subscriber.payload.EmailAddress}`);
        }
        else {
            toast.success(`subscription already exists for: ${this.props.subscriber.payload.EmailAddress}`);
        }
        this.props.reset();
        this.props.resetSubscription();
    }

    //destructure error and touched came out of meta. touched will be true, only if the field was interacted with by a user
    //and then the user clicks away from the field. So now show the error if there is an error.
    renderError = ({ error, touched }) => {
        if (error && touched) {
            return (
                <div className="ui error message">
                    <div className="header">{error}</div>
                </div>
            );
        }
    }

    renderInput = ({ input, label, meta }) => {
        return (
            <div className="field">
                <label style={{ width: '100px' }}>{label}</label>
                <input {...input} />
                {this.renderError(meta)}
            </div>
        );
    }

    onSubmit = (formValues) => {
        //this will cause a re-render, and componentDidUpdate will intercept it.
        this.props.createSubscriber(formValues);
    }

    render() {
        //console.log("props " + JSON.stringify(this.props))<div className="container center">
        return (
            <div className="parent" align="center">
                <fieldset className="customLegend1 child signup-inline-block-child col-4">
                    <legend>NEWSLETTER SIGN-UP:</legend>

                    <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form error">
                        <div className="form-group row">
                            <div className="col-12 col-s-9">
                                <Field name="FirstName" component={this.renderInput} label="First Name" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-12 col-s-9">
                                <Field name="LastName" component={this.renderInput} label="Last Name" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-12 col-s-9">
                                <Field name="EmailAddress" component={this.renderInput} label="Email Address" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-12 col-s-9">
                                <Field name="ConfirmEmail" component={this.renderInput} label="Confirm Email" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-12 col-s-9">
                                <button style={{ position: 'relative', top: '1vh', }} className="ui button primary" id="register">Submit</button>
                            </div>
                        </div>
                    </form>
                </fieldset>

                <div className={this.props.spinnerIsHidden ? 'dimSpinner signUpSpinner' : 'show signUpSpinner'} id="loading-spinner"></div>

                <fieldset className="left-align-text customLegend2 child signup-inline-block-child col-4">
                    <legend>UpComing Events:</legend>
                    <p className="eventmessages">Birding at White Rock Lake</p>
                    <p className="eventmessages">Birding at Univ Southwest med center rookery</p>
                    <p className="eventmessages">Birding at Village creek drying beds</p>
                    <p className="eventmessages">Birding at Bosque del Apache in New Mexico</p>
                    <p className="eventmessages">Native Texas wildflowers</p>
                </fieldset>
            </div >
        )
    }
}

//This module is outside of the SignUp class module. It is hooked up to everything via the reduxForm method down below.
//This validation code is required by reduxForm.
//this code runs before the component has rendered the first time.
//It then runs each time the user starts to make entries into the form fields.
//Each keystroke by the user invokes an action and thus a re-render (via redux-forms behind the scenes).
//NOTE: each of the !formValues.X are in fact undefined at the first run of this code, but defined thereafter.
//So the document.getElementById('register') code breaks on the inital run if you don't check for null
//prior to the component renderring the first time, but then document.getElementById('register') works
//there after.
const validateForm = formValues => {
    const errors = {};

    const enableSubmit = () => {
        const signupSubmit = document.getElementById('register');
        signupSubmit.className = 'showing';
    };

    const disableSubmit = () => {
        const signupSubmit = document.getElementById('register');
        if (signupSubmit !== null) {
            signupSubmit.className = 'dimSignupSubmit';
        }
    };

    const validEmailAddress = (value) => {
        return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
            ? 'Invalid email address'
            : undefined
    }

    if (!formValues.FirstName) {
        errors.FirstName = 'You must enter a firstName';
        disableSubmit();
    }

    if (!formValues.LastName) {
        errors.LastName = 'You must enter a lastName';
        disableSubmit();
    }

    if (!formValues.EmailAddress) {
        errors.EmailAddress = 'You must enter an EmailAddress';
        disableSubmit();
    }

    if (!formValues.ConfirmEmail) {
        errors.ConfirmEmail = 'You must enter a ConfirmEmail';
        disableSubmit();
    }


    if (formValues.FirstName && formValues.LastName) {
        if (formValues.EmailAddress && formValues.ConfirmEmail) {
            if (formValues.EmailAddress === formValues.ConfirmEmail) {
                errors.EmailAddress = validEmailAddress(formValues.EmailAddress);
                errors.ConfirmEmail = validEmailAddress(formValues.ConfirmEmail);
                if (!errors.EmailAddress && !errors.ConfirmEmail) {
                    enableSubmit();
                }
                return errors;
            }
            else {
                errors.EmailAddress = "Your EmailAddress doesn't match your confirm emailAddress";
                disableSubmit();
            }
        }
    }

    return errors;
};

//this performs the same task as map state to props, reduxForm EXPECTS there to be a validate prop and a form prop!
const formWrapped = reduxForm({
    form: 'SignUp',
    validate: validateForm
})(SignUp);
const mapStateToProps = (state) => {
    //console.log('mapStateToProps ' + state)
    return { subscriber: state.subscribers, spinnerIsHidden: state.spinnerVisibility.signupSpinnerIsHidden };
}
export default connect(mapStateToProps, { createSubscriber: createSubscriber, resetSubscription: resetSubscription })(formWrapped);