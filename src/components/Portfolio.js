import React from 'react';
import { connect } from 'react-redux';
import Gallery from './gallery/Gallery';
import PortfolioNavBtns from './gallery/PortfolioNavBtns';


class Portfolio extends React.Component {




    selection = (btn) => {
        //alert('selection made ' + btn);
        let nameArray, i;
        nameArray = document.getElementsByClassName("column"); 
        if (btn === "all") btn = "";
        for (i = 0; i < nameArray.length; i++) {
            this.RemoveClass(nameArray[i]); 
            if (nameArray[i].className.indexOf(btn) < 0) this.AddHideClass(nameArray[i])
        }
    };

    AddHideClass = (element) => {
        element.className += " hide";
    };
    //remove the show and hide class everywhere they are found.
    //(results in all of the images to show by default)
    RemoveClass = (element) => {
        let arr1 = element.className.split(" ");

        while (arr1.indexOf("hide") > -1) {
            arr1.splice(arr1.indexOf("hide"), 1);
        }
        element.className = arr1.join(" ");
    };


    hidePortfolioLink = () => {
        let portfolioLink = document.getElementById('portfolio-link');
        portfolioLink.className = 'dimNavLink';
    };


    componentDidMount = () => {
        const btns = {
            btn1: "all",
            btn2: "birds",
            btn3: "butterflies",
            btn4: "insects",
            btn5: "flora",
            btn6: "landscape"
        }

        console.log("Initializing Photography.portfolio...");
        let btn1 = document.getElementById('btn1');
        let btn2 = document.getElementById('btn2');
        let btn3 = document.getElementById('btn3');
        let btn4 = document.getElementById('btn4');
        let btn5 = document.getElementById('btn5');
        let btn6 = document.getElementById('btn6');

        btn1.onclick = () => {
            this.selection(btns.btn1);
            btn1.className = btn1.className.replace(" hide", " show");
            btn2.className = btn2.className.replace(" hide", " show");
            btn3.className = btn3.className.replace(" hide", " show");
            btn4.className = btn4.className.replace(" hide", " show");
            btn5.className = btn5.className.replace(" hide", " show");
            btn6.className = btn6.className.replace(" hide", " show");
        };
        btn2.onclick = () => {
            this.selection(btns.btn2);
            btn3.className = btn3.className.replace("show", "hide");
            btn4.className = btn4.className.replace("show", "hide");
            btn5.className = btn5.className.replace("show", "hide");
            btn6.className = btn6.className.replace("show", "hide");

        };
        btn3.onclick = () => {
            this.selection(btns.btn3);
            btn2.className = btn2.className.replace("show", "hide");
            btn4.className = btn4.className.replace("show", "hide");
            btn5.className = btn5.className.replace("show", "hide");
            btn6.className = btn6.className.replace("show", "hide");
        };
        btn4.onclick = () => {
            this.selection(btns.btn4);
            btn2.className = btn2.className.replace("show", "hide");
            btn3.className = btn3.className.replace("show", "hide");
            btn5.className = btn5.className.replace("show", "hide");
            btn6.className = btn6.className.replace("show", "hide");
        };
        btn5.onclick = () => {
            this.selection(btns.btn5);
            btn2.className = btn2.className.replace("show", "hide");
            btn3.className = btn3.className.replace("show", "hide");
            btn4.className = btn4.className.replace("show", "hide");
            btn6.className = btn6.className.replace("show", "hide");
        };
        btn6.onclick = () => {
            this.selection(btns.btn6);
            btn2.className = btn2.className.replace("show", "hide");
            btn3.className = btn3.className.replace("show", "hide");
            btn4.className = btn4.className.replace("show", "hide");
            btn5.className = btn5.className.replace("show", "hide");
        };

        this.hidePortfolioLink();
        this.selection("all");

        //this is an additional click event (above and beyond the natural click event that DOM elements already have)
        //we need this click event handler to deal with toggling the 'active' class
        const navBtns = [...document.getElementsByClassName('btn')];
        navBtns.forEach(function (btn, index) {
            //alert('register index ' + index);
            btn.addEventListener("click", function () {
                //returns an array of 'active', but, there will only ever be one in our case
                let current = document.getElementsByClassName("active");
                //remove the 'active' class
                current[0].className = current[0].className.replace(" active", "");
                //now set 'active class' on the btn that was clicked
                btn.className += " active";
            });
        });
    }

    render() {
        return (
            <div>
                <Gallery images={this.props.portfolioImages}>
                    <PortfolioNavBtns />
                </Gallery>
            </div >
        );
    }
}


const mapStateToProps = (state) => {
    return {
        portfolioImages: state.portfolioImages
    };
}

export default connect(mapStateToProps)(Portfolio);


