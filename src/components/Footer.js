import React from 'react';
import SignUp from './SignUp';

class Footer extends React.Component {

    getDate = () => {
        const d = new Date();
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        const month = months[d.getMonth()];


        const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        const day = days[d.getDay()];

        const year = d.getFullYear();

        const dd = d.getDate().toString();
        const ddChars = dd.split('');
        const dayNumber = (ddChars[1] ? dd : "0" + ddChars[0])



        return `${day} ${month} ${dayNumber} ${year}`
    }


    render() {
        return (
            <div id="footer" align="center" >
                <div className="center">&copy; Copyright Protected</div>
                <div >
                    <hr className="navbar set-hr-width" />
                    <p className="ui container">
                        All of the images in this website are copyrighted original works by Chris Marek, and they are protected by the United States and international copyright law.
                        Use of any content from this site, for any purpose, is strictly forbidden without express, written permission from Chris Marek Photography.
                    </p>
                    <div align="center">
                        <SignUp />
                        <p>&copy;{this.getDate()} - Chris Marek Photography</p>
                        <div>
                            <div><a href="https://www.flickr.com/photos/30888088@N02/">Visit my photos on flickr</a></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;