import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import About from './About';
import Home from './Home';
import Images from './Images';
import Portfolio from './Portfolio';
import Contact from './Contact';
import NavBar from './NavBar';
import Footer from './Footer';
import { ToastContainer } from 'react-toastify';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


import createBrowserHistory from '../history'; //puts us in charge of the history object (url history that is)

class App extends React.Component {
    componentDidMount() {
        const protectCopyright = (event) => {
            toast.info("copy right protected");
            event.preventDefault();
        };
        document.body.addEventListener('contextmenu', protectCopyright);
    }


    render() {
        return (
            // <div className="ui container"> the container class will narrow the field of view of 
            //the website so that there is a large amount of blank space on either side. So I leave it off for now
            <div className="ui">
                <Router history={createBrowserHistory}>
                    <div align="center">
                        <ToastContainer />
                        <NavBar />
                        <Switch>
                            <Route path="/" exact component={Home} />
                            <Route path="/Portfolio" exact component={Portfolio} />
                            <Route path="/Images" exact component={Images} />
                            <Route path="/About" exact component={About} />
                            <Route path="/Contact" exact component={Contact} />
                        </Switch>
                        <Footer />
                    </div>
                </Router >
            </div >
        )
    }
}
export default App;

