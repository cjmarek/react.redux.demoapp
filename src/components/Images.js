import React from 'react';
import { connect } from 'react-redux';
import Gallery from './gallery/Gallery';
import ImagesDropdownNav from './gallery/ImagesDropdownNav';
import { dropdownSelection } from '../actions'

class Images extends React.Component {
    setSelected = (option) => {
        this.props.dropdownSelection(option);
    };
    componentDidMount() {
        console.log("Initializing Photography.images..");
        let imageslink = document.getElementById('images-link');
        imageslink.className = 'dimNavLink disabled';
    }


    render() {
        return (
            <div>
                <Gallery images={this.props.dropdownImages}>
                    <ImagesDropdownNav
                        label="Select a species"
                        selected={this.props.dropdownSelection}
                        onSelectedChange={this.setSelected}
                        options={this.props.dropdownOptions} />
                </Gallery>
            </div >
        )
    }
};


const mapStateToProps = (state) => {
    return {
        dropdownSelection: state.dropdownSelection.selection,
        dropdownImages: state.dropdownImages,
        dropdownOptions: state.dropdownOptions
    };
}
//NOTE action creator 'dropdownSelection' included here
export default connect(mapStateToProps, { dropdownSelection: dropdownSelection })(Images);