import React from 'react';
//connect can also be used with functional components if need be
import { connect } from 'react-redux';
//this Link is a component that we use instead of using anchor tags. It is a way to change the route, which is defined by App.js
//anchor tags are bad because they cause a full page postback. Link allows for Single page applications
import { Link } from 'react-router-dom';
import { setSlideshowSpinnerIsHidden } from '../actions';


//need to hook up an action creator via a click to cause the state to change and then the whole app will re-render
const NavBar = (props) => {
    const onClick = (event) => {
        var linkName = event.target + ""; //converts event.target to a string by concat a string to it

        let portfolioLink = document.getElementById('portfolio-link');
        let imagesLink = document.getElementById('images-link');
        let contactLink = document.getElementById('Contact-link');
        let aboutLink = document.getElementById('about-link');
        let slideShowlink = document.getElementById('slideshow-link');
        portfolioLink.className = 'dimNavLink showing';
        imagesLink.className = 'dimNavLink showing';
        contactLink.className = 'dimNavLink showing';
        aboutLink.className = 'dimNavLink showing';
        slideShowlink.className = 'dimNavLink showing';
    };


    return (
        <div>
            <div>
                <a className="left-align-text" href="https://www.flickr.com/photos/30888088@N02/">&#8592;  Return to flickr</a>
                <img src="photography/HomeBanner.png" alt="chris marek photography" width="80%" height="100%" />
            </div>
            <div align="center" >
                <Link onClick={onClick} to="/" className="item photography-nav slideshow-link" id="slideshow-link">
                    Home
                </Link>
                <Link onClick={onClick} to="/Portfolio" className="item photography-nav portfolio-link" id="portfolio-link">
                    Portfolio
                 </Link>
                <Link onClick={onClick} to="/Images" className="item photography-nav images-link" id="images-link">
                    Images
                </Link>
                <Link onClick={onClick} to="/About" className="item photography-nav about-link" id="about-link">
                    About
                </Link>
                <Link onClick={onClick} to="/Contact" className="item photography-nav Contact-link" id="Contact-link">
                    Contact
                </Link>
                <hr className="navbar set-hr-width" />
            </div >
        </div >
    );
};
const mapStateToProps = (state) => {
    return { currentSlide: state.fetchCurrentSlideReducer.currentSlide };
}
export default connect(mapStateToProps, { setSlideshowSpinnerIsHidden: setSlideshowSpinnerIsHidden })(NavBar);