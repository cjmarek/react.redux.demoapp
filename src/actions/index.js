import subscribers from '../apis/subscribers';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { SHOW_MODAL, DROPDOWN_SELECTION, DROPDOWN_OPEN, CURRENT_SLIDE, CREATE_SUBSCRIBER, RESET_SUBSCRIBER, SLIDESHOW_SPINNER_VISIBILITY, SIGNUP_SPINNER_VISIBILITY } from './types';


export const showModal = (bool, document, description, index) => {
    return {
        type: SHOW_MODAL,
        payload: { isOpen: bool, document: document, description: description, index: index }
    };
};

export const dropdownSelection = (selection) => {
    return {
        type: DROPDOWN_SELECTION,
        payload: { selection: selection }
    };
};

export const dropdownOpen = (bool) => {
    return {
        type: DROPDOWN_OPEN,
        payload: { isOpen: bool }
    };
};

export const setSlideshowSpinnerIsHidden = (bool) => {
    return {
        type: SLIDESHOW_SPINNER_VISIBILITY,
        payload: { slideshowSpinnerIsHidden: bool }
    };
};

export const setSignupSpinnerIsHidden = (bool) => {
    return {
        type: SIGNUP_SPINNER_VISIBILITY,
        payload: { signupSpinnerIsHidden: bool }
    };
};

//I want the slideshow to pickup where ever it left off.
export const setCurrentSlide = (currentSlide) => {
    return {
        type: CURRENT_SLIDE,
        payload: { currentSlide: currentSlide }
    };
};


export const createSubscriber = formValues => async dispatch => {
    let response, resultFound, cns;
    dispatch({ type: SIGNUP_SPINNER_VISIBILITY, payload: { signupSpinnerIsHidden: false } });
    try {
        response = await subscribers.get('/subscribers');
        //axios always returns a ton of data on the response, we only care about the one called 'data'
        resultFound = response.data.find(subscriber => subscriber.formValues.EmailAddress === formValues.EmailAddress);
        cns = !resultFound ? true : false;
        //response is an echo of the data that was posted. I am not using response. Just demonstrating it can be returned if need be.
        //If the post failed, response can be tested to determine if the post succeeded.
        if (cns === true) {
            response = await subscribers.post('/subscribers', { formValues });
        }
        //need to change the local state to reflect whether the subscriber got registered or was already registered, cns = false means they are already registered.
        dispatch({ type: CREATE_SUBSCRIBER, payload: { fetchSubscriptionAttempted: true, createNewSubscription: cns, payload: formValues } });
        dispatch({ type: SIGNUP_SPINNER_VISIBILITY, payload: { signupSpinnerIsHidden: true } });
    } catch (error) {
        console.log(error);
        toast.warn(`Newsletter registration failed`);
        toast.error(`Network is down: ${error}`);
        dispatch({ type: SIGNUP_SPINNER_VISIBILITY, payload: { signupSpinnerIsHidden: true } });
    }
};


export const resetSubscription = () => {
    return {
        type: RESET_SUBSCRIBER
    };
};
//we will not use this next statement declaration because our actions index.js file is going to hold
//many actions, not just one. So we will put the export keyword on each function declaration
//so that the action(s) behaves as a Named export(s)
//export default fetchSlideShow