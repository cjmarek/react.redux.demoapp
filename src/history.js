//this is our own custom history object.

//the idea here is that I am going to maintain this history object as opposed
//to BrowserRouter creating the history object internally. Because I am creating this, 
//it will be a lot easier to have access to it and thereby change what page (component) the user is looking at.
//The history package was installed automatically as a dependancy of React-Router-DOM
//So, in App.js, we replace BrowserRouter tag with Router tag (plain ol router). So heck, if this
//is more versitile, why would we ever use BrowserRouter?
import { createBrowserHistory } from 'history';
//createBrowserHistory will create a history object that I maintain and I can access from anywhere in my project
//because I can import it into where ever it is needed.
export default createBrowserHistory();