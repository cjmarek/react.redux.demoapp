import React from "react";
//connect can also be used with functional components if need be
import { connect } from 'react-redux';
import { showModal } from '../../actions';
import Modal from "react-modal";


Modal.setAppElement("#root");

//Gallery is being used by the Portfolio component and the Images (dropdown) component.
//The Images component is really the Dropdown page.
class Gallery extends React.Component {

    toggleModal = () => {
        this.props.showModal(!this.props.isOpen, '', '', '');
    };

    onViewDocument = (document, description, index) => {
        //invoke showModel action creator with this.props.showModal and 4 parameters
        this.props.showModal(!this.props.isOpen, document, description, index);
    };

    imagesFilter = (image) => {
        let bool = false;
        switch (window.location.pathname) {
            case "/Portfolio":
                bool = true;
                break;
            case "/Images":
                if (this.props.dropdownSelection === image.description) {
                    bool = true;
                }
                break;
            default: {
                bool = false;
            }
        }
        return bool;
    }


    renderList = () => {
        //DO NOT REMOVE THIS React.Fragment
        return <React.Fragment>{this.props.images.map((image) => {
            if (this.imagesFilter(image) === false) { return null; };
            return (
                <li key={image.index} className={`column imageDescription portfolio ${image.type} ${image.specifictype}`}>
                    <a href="#!">
                        <img onClick={() => this.onViewDocument(`${image.imagepath}${image.index}a.jpg`, `${image.description}`, `${image.index}`)}
                            src={`${image.imagepath}${image.index}a.jpg`}
                            alt={`${image.description}`}
                            className="portfolioImage" />
                    </a>
                    <div style={{ width: '100px' }} className="breakword portfolioOverlay">{`${image.description} (${image.index})`}</div>
                </li>
            );
        })}</React.Fragment>
    }


    render() {
        return (
            <div align="center">
                <Modal
                    isOpen={this.props.isOpen}
                    onRequestClose={this.toggleModal}
                    contentLabel="My dialog"
                    className="mymodal"
                    overlayClassName="myoverlay"
                    closeTimeoutMS={500}
                >
                    <img src={this.props.document} alt={`${this.props.description}`} className="modalImage" />
                    <div style={{ position: 'relative', top: '1vh', }} align="center">
                        <button onClick={this.toggleModal}>Close</button>
                    </div>
                    <div style={{ position: 'relative', top: '1vh', }} align="center">
                        {`${this.props.description} (${this.props.index})`}
                    </div>
                </Modal>
                {/* Navigation components go here */}
                <div>{this.props.children}</div>

                <div>
                    <ul className="no-bullets">
                        {this.renderList()}
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isOpen: state.modal.isOpen,
        document: state.modal.document,
        description: state.modal.description, index: state.modal.index,
        dropdownSelection: state.dropdownSelection.selection
    };
}
//NOTE action creators included here
export default connect(mapStateToProps, { showModal: showModal })(Gallery);





