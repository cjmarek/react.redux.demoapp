import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { dropdownOptions, dropdownImages, portfolioImages, slideshowImages } from './images';
import { SHOW_MODAL, DROPDOWN_SELECTION, DROPDOWN_OPEN, CURRENT_SLIDE, CREATE_SUBSCRIBER, RESET_SUBSCRIBER, SLIDESHOW_SPINNER_VISIBILITY, SIGNUP_SPINNER_VISIBILITY } from '../actions/types';

const modalPayloadDefault = { isOpen: false, document: "my data here", description: " ", index: 0 };
const dropdownPayloadDefault = { selection: "Great blue heron" };
const dropdownOpenPayloadDefault = { isOpen: false };
const currentSlidePayloadDefault = { currentSlide: -1 };
const subscriptionFoundPayloadDefault = { fetchSubscriptionAttempted: false, createNewSubscription: false, payload: {} };
const spinnerPayloadDefault = { slideshowSpinnerIsHidden: window.location.pathname === '/' ? false : true, signupSpinnerIsHidden: true };

const dropdownOptionsReducer = (state = dropdownOptions) => {
    return state;
};

const dropdownImagesReducer = (state = dropdownImages) => {
    return state;
};

const portfolioImagesReducer = (state = portfolioImages) => {
    return state;
};

const slideshowImagesReducer = (state = slideshowImages) => {
    return state;
};

const fetchCurrentSlideReducer = (state = currentSlidePayloadDefault, action) => {
    if (action.type === CURRENT_SLIDE) {
        return { ...state, ...action.payload };
    }
    return state;
};

const modalReducer = (state = modalPayloadDefault, action) => {
    if (action.type === SHOW_MODAL) {
        return { ...state, ...action.payload };
    }
    return state;
};

const dropdownSelectionReducer = (state = dropdownPayloadDefault, action) => {
    if (action.type === DROPDOWN_SELECTION) {
        return { ...state, ...action.payload };
    }
    return state;
};

const dropdownOpenReducer = (state = dropdownOpenPayloadDefault, action) => {
    if (action.type === DROPDOWN_OPEN) {
        return { ...state, ...action.payload };
    }
    return state;
};


const setSpinnerVisibilityReducer = (state = spinnerPayloadDefault, action) => {
    if (action.type === SLIDESHOW_SPINNER_VISIBILITY) {
        return { ...state, ...action.payload }
    }
    if (action.type === SIGNUP_SPINNER_VISIBILITY) {
        return { ...state, ...action.payload }
    }
    return state;
};


const subscriberReducer = (state = subscriptionFoundPayloadDefault, action) => {
    switch (action.type) {
        case CREATE_SUBSCRIBER:
            return action.payload;
        case RESET_SUBSCRIBER:
            return subscriptionFoundPayloadDefault;
        default:
            return state;
    }
};


export default combineReducers({
    form: formReducer,
    portfolioImages: portfolioImagesReducer,
    dropdownImages: dropdownImagesReducer,
    dropdownOptions: dropdownOptionsReducer,
    slideshowImages: slideshowImagesReducer,
    modal: modalReducer,
    dropdownSelection: dropdownSelectionReducer,
    dropdownIsOpen: dropdownOpenReducer,
    //currentSlide: fetchCurrentSlideReducer,  //see my document ExplainingReducers.txt
    fetchCurrentSlideReducer,      // I like doing it this way better, you don't end up with code that looks like currentSlide.currentSlide out in the application
    subscribers: subscriberReducer,
    spinnerVisibility: setSpinnerVisibilityReducer
});



// import { combineReducers } from 'redux';

// export default combineReducers( {
//    replaceMe: () => 'replace this dummy reducer'
// });