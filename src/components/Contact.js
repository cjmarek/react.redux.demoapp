import React from 'react';

class Contact extends React.Component {
    componentDidMount() {
        function hideContactlink() {
            let contactLink = document.getElementById('Contact-link');
            contactLink.className = 'dimNavLink';
        };

        hideContactlink();
    }

    render() {
        return (
            <div className="center">
                <p>
                    <b>Contact : cjmarek1@gmail.com</b>
                </p>
                <p>Enjoy Nature!</p>
                <br></br>
            </div>
        );
    }
}

export default Contact;