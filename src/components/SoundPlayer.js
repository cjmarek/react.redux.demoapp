 import React, { useState, useEffect, useRef } from 'react';

const SoundPlayer = ({src}) => {

  const [isPlaying, setIsPlaying] = useState(false);
  const audioRef = useRef(null);

  useEffect(() => {
    const audio = audioRef.current;

    const handleEnded = () => {
      audio.currentTime = 0;
      audio.play().catch(error => console.error("Playback failed:", error));
    };

    if (isPlaying) {
      audio.play().catch(error => console.error("Playback failed:", error));
      audio.addEventListener('ended', handleEnded);
    } else {
      audio.pause();
      audio.removeEventListener('ended', handleEnded);
    }

    return () => {
      audio.pause();
      audio.removeEventListener('ended', handleEnded);
    };
  }, [isPlaying, src]);

  const togglePlay = () => {
    setIsPlaying(!isPlaying);
  };

  return (
    <div>
      <audio ref={audioRef} src={src} preload="auto" />
      <button onClick={togglePlay}>{isPlaying ? `Stop Music`: `Play Music`}</button>
    </div>
  );
}

export default SoundPlayer; 
