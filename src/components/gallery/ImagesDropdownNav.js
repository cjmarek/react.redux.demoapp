import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { dropdownOpen } from '../../actions';

//we want this component to be able to listen for click events that happen outside of any ui elements that make
//up the drop down. we use useRef to enable doing that. We need a way to determine what element was clicked so that
//we can decide if the click originated inside the dropdown. Event.target tells us which element was clicked
const Dropdown = (props) => {
    const ref = useRef();
    //the so called 'hooks' useEffect function
    useEffect(() => {
        const onBodyClick = (event) => {

            //ref.current gets set to null if we removed the dropDown component, which would then break this code
            if (ref.current && ref.current.contains(event.target)) {
                return;
            }
            //invoke action creator
            props.dropdownOpen(false);
        };
        //note : this is normal javascript, not React
        //also note, this event listener comes first in the bubble up because it was manually assigned which take precedense (as opposed to natural click event)
        document.body.addEventListener('click', onBodyClick);
        //the cleanup function that gets invoked on the 'second' time thru and thereafter
        return () => {
            document.body.removeEventListener('click', onBodyClick);
        };
    }, [props]);

    const dropdownOpen = (bool) => {
        props.dropdownOpen(bool);
    };

    const renderedOptions = () => {
        return props.options.map((option) => {
            //this skips the selected item from appearing in the dropdown list since it is already selected
            if (option.value === props.dropdownSelection) { return null; }
            return (
                <div key={option.value}
                    className="item"
                    onClick={() => props.onSelectedChange(option.value)}>{/* this will invoke setSelected on Images passing back this option*/}
                    {option.label}
                </div>
            );
        });
    }

    //console.log(ref.current);

    return (
        <div ref={ref} className="ui container form">
            <div className="field">
                <label className="label">{props.label}</label>
                <div
                    onClick={() => dropdownOpen(!props.isDropdownOpen)}
                    className={`ui selection dropdown ${props.isDropdownOpen ? ' visible active' : ""}`}>
                    <i className="dropdown icon"></i>
                    <div className="text">{props.dropdownSelection}</div>
                    <div className={`menu ${props.isDropdownOpen ? ' visible transition' : ''}`}>{renderedOptions()}</div>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return { dropdownSelection: state.dropdownSelection.selection, isDropdownOpen: state.dropdownIsOpen.isOpen };
}
//NOTE action creator included here
export default connect(mapStateToProps, { dropdownOpen: dropdownOpen })(Dropdown);