import React from 'react';
//connect can also be used with functional components if need be
import { connect } from 'react-redux';

import { setCurrentSlide, setSlideshowSpinnerIsHidden } from '../../actions';


class SlideShow extends React.Component {
    //notice that I just whip up a property on the 'this' and call it slideInterval
    //as a general use variable to be used to kill the timer on un-mount of component.
    //'this.state' would be used in this location, and initialized here, (I am not using it in this example)
    //this.state, if you use that, can only be initialized with setState, and causes re-renders.
    //NOTE: props is the same props object that we used in functional components.
    constructor(props) {
        super(props);
        //this.state = { spinnerDisplay: 'show' };
        this.slideInterval = 0;
        this.currentSlide = this.props.currentSlide;
        this.playing = true;
        //can't do this here because the DOM has not been rendered yet,
        //thats why componentDidMount is the place to look into the DOM.
        //this.pauseButton = document.getElementById('pause');
    };

    nextSlide = () => {
        this.goToSlide(this.currentSlide + 1);
    };

    previousSlide = () => {
        this.goToSlide(this.currentSlide - 1);
    };

    //the show / hide functionality takes place now in the renderList function.
    //the only purpose here is to update the redux store state for currentSlide
    //so that we can pick back up on whatever the last image was before we left the slide show.
    goToSlide = (n) => {
        let slides = document.querySelectorAll('.slides .slide');
        //this statement figures out when it is time to set the count back to zero
        this.currentSlide = (n + slides.length) % slides.length;
        //this will cause all components to re-render. the slide show and spinner components to re-render,
        //(which will cause the  {this.renderList()} to happen, and that is what puts  class 'slide showing' on a image)
        this.props.setCurrentSlide(this.currentSlide);
        this.hideSlideShowSpinner();
    };

    pauseSlideshow = () => {
        let pauseButton = document.getElementById('pause');
        pauseButton.innerHTML = 'Play';
        this.playing = false;
        clearInterval(this.slideInterval);
    };

    playSlideshow = () => {
        let pauseButton = document.getElementById('pause');
        pauseButton.innerHTML = 'Pause';
        this.playing = true;
        this.slideInterval = setInterval(this.nextSlide, 2000);
    };


    hideSlideShowSpinner = () => {
        if (this.currentSlide > -1) {
            this.props.setSlideshowSpinnerIsHidden(true);
        }
    };

    showSlideShowSpinner = () => {
        this.props.setSlideshowSpinnerIsHidden(false);
    };

    hideSlideShowlink = () => {
        let slideShowlink = document.getElementById('slideshow-link');
        slideShowlink.className = 'dimNavLink';
    };

    //this gets called for the first render of this component, and never again.
    //The DOM finally exists at the time this gets called so this is 
    //a good place to wire up events on UI elements.
    //So this is the same thing as document.Ready in jQUERY.
    //The above statement is true, but confusing. What it is saying is,
    //while this component is currently being displayed on the screen, it
    //will re-render everytime the state changes, but the componentDidMount
    //was executed after the first, of any renders, that this component will undergo.
    //When you navigate away from the current component, the component is unmounted from the DOM.
    //That is why componentWillUnmount() is invoked, and is why componentDidMount()
    //will be invoked once again when we navigate back to this component.
    componentDidMount = () => {
        const change_image_time = 5000;

        //kick off slideshow
        this.slideInterval = setInterval(this.nextSlide, change_image_time);

        //wire up buttons references
        let next = document.getElementById('next');
        let previous = document.getElementById('previous');
        let pauseButton = document.getElementById('pause');
        let restartButton = document.getElementById('restart');

        next.onclick = () => {
            this.pauseSlideshow();
            this.nextSlide();
        };

        previous.onclick = () => {
            this.pauseSlideshow();
            this.previousSlide();
        };

        pauseButton.onclick = () => {
            if (this.playing) {
                this.pauseSlideshow();
            } else {
                this.playSlideshow();
            }
        };

        restartButton.onclick = () => {
            clearInterval(this.slideInterval);
            this.currentSlide = -1;
            this.showSlideShowSpinner();
            this.props.setCurrentSlide(this.currentSlide);
            this.playSlideshow();
        };

        this.hideSlideShowlink();

    }
    //Need to kill the timer.
    //Otherwise the timer keeps running behind the scenes no matter which component is being viewed.
    //When you navigate away from the current component, the component is unmounted from the DOM.
    //That is why componentWillUnmount() is invoked.
    componentWillUnmount = () => {
        clearInterval(this.slideInterval);
        this.props.setSlideshowSpinnerIsHidden(true);
    }

    renderList = () => {

        return <div>{this.props.slideshowImages.map((image, index) => {
            if (index === this.props.currentSlide) {
                return (
                    <li key={image.index} className="imageDescription slide showing" >
                        <img src={`${image.imagepath}${image.index}a.jpg`} alt={`${image.description}`} className="responsive" />
                        <div className="slideshowOverlay">{`${image.description} (${image.index})`}</div>
                    </li>
                );
            }

            return (
                <li key={image.index} className="imageDescription slide" >
                    <img src={`${image.imagepath}${image.index}a.jpg`} alt={`${image.description}`} className="responsive" />
                    <div className="slideshowOverlay">{`${image.description} (${image.index})`}</div>
                </li>
            );
        })}</div>
    }

    render() {
        return (
            <div>
                <ul className="slides" >
                    <div className={this.props.spinnerIsHidden ? 'dimSpinner loader1' : 'show loader1'} id="loading-spinner"></div>
                    {this.renderList()}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { slideshowImages: state.slideshowImages, currentSlide: state.fetchCurrentSlideReducer.currentSlide, spinnerIsHidden: state.spinnerVisibility.slideshowSpinnerIsHidden };
}
//NOTE action creator(s) included here
export default connect(mapStateToProps, { setCurrentSlide: setCurrentSlide, setSlideshowSpinnerIsHidden: setSlideshowSpinnerIsHidden })(SlideShow);






