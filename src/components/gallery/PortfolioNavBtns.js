import React from 'react';

const PortfolioNavBtns = () => {

    return (
        <div>
            <h2>PORTFOLIO</h2>
            <div id="btnContainer">
                <button id="btn1" className="btn active show"> Show all</button>
                <button id="btn2" className="btn show"> Birds</button>
                <button id="btn3" className="btn show"> Butterflies</button>
                <button id="btn4" className="btn show"> Insects</button>
                <button id="btn5" className="btn show"> Flora</button>
                <button id="btn6" className="btn show"> Landscapes</button>
            </div >
        </div >
    );
}

export default PortfolioNavBtns;
